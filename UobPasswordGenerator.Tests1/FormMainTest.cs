// <copyright file="FormMainTest.cs">Copyright ©  2019</copyright>
using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UobHashSalt;

namespace UobHashSalt.Tests
{
    /// <summary>This class contains parameterized unit tests for FormMain</summary>
    [PexClass(typeof(FormMain))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class FormMainTest
    {
    }
}
