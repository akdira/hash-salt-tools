﻿using EncryptionStandards;
using System;

namespace EncyptionConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Method
                var method = args[0].Remove(0,1).ToLower();

                // Key
                var key = args[2];

                // Salt
                var salt = args[4];

                // Plain
                var plain = args[6];

                //Console.WriteLine("Method:");
                //Console.WriteLine(method);

                //Console.WriteLine("Key:");
                //Console.WriteLine(key);

                //Console.WriteLine("Salt:");
                //Console.WriteLine(salt);

                //Console.WriteLine("Plain:");
                //Console.WriteLine(plain);

                string result = "";

                if (method == "encrypt")
                {
                    result = AesEncryption.Encrypt(plain, key, salt);
                }
                else if (method == "decrypt")
                {
                    result = AesEncryption.Decrypt(plain, key, salt);
                }
                else
                {
                    throw new Exception("Method not found");
                }

                Console.WriteLine(result);

                //Console.ReadLine();

            }
            catch (Exception error)
            {
                Console.WriteLine("Error: " + error.Message);
                //Console.ReadLine();
            }
        }
    }
}
