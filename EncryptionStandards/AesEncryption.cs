﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace EncryptionStandards
{
    public class AesEncryption
    {

        public static string Encrypt(string plainText, string KEY, string SALT)
        {
            byte[] secretKey = null;
            using (Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(KEY, Encoding.UTF8.GetBytes(SALT), 65536, HashAlgorithmName.SHA256))
            {
                secretKey = rfc2898.GetBytes(32);
            }

            using (RijndaelManaged AES = new RijndaelManaged())
            {
                AES.Key = secretKey;
                AES.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;

                byte[] encryptedData;
                using (ICryptoTransform encryptor = AES.CreateEncryptor())
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                            {
                                streamWriter.Write(plainText);
                            }
                            encryptedData = memoryStream.ToArray();
                        }
                    }
                }

                return Convert.ToBase64String(encryptedData);
            }
        }

        public static string Decrypt(string encryptedText, string KEY, string SALT)
        {
            byte[] secretKey = null;
            using (Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(KEY, Encoding.UTF8.GetBytes(SALT), 65536, HashAlgorithmName.SHA256))
            {
                secretKey = rfc2898.GetBytes(32);
            }

            using (RijndaelManaged AES = new RijndaelManaged())
            {
                AES.Key = secretKey;
                AES.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                AES.Mode = CipherMode.CBC;
                AES.Padding = PaddingMode.PKCS7;

                string decryptedText;
                using (ICryptoTransform decryptor = AES.CreateDecryptor())
                {
                    using (MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(encryptedText)))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader(cryptoStream))
                            {
                                decryptedText = streamReader.ReadToEnd();
                            }
                        }
                    }
                }

                return decryptedText;
            }
        }
    }
}
