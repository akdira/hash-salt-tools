using EncryptionStandards;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Threading;
using UniqueKey;

namespace EncryptDecrypt2WayTest
{
    [TestClass]
    public class EncryptionTest
    {
        [TestMethod]
        public void EncryptDecryptClassTest()
        {
            string PlainText = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);
            string Key = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);
            string Salt = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);

            string Encrypted_string = AesEncryption.Encrypt(PlainText, Key, Salt);
            string Decrypted_string = AesEncryption.Decrypt(Encrypted_string, Key, Salt);

            Assert.AreEqual(PlainText, Decrypted_string);

        }

        [TestMethod]
        public void EncryptDecryptConsoleTest()
        {
            string PathToConsole = $@"E:\SC\UobHashSalt_3 - Unit Test\EncyptionConsole\bin\Release\netcoreapp2.2\win-x64\EncyptionConsole.exe";


            string PlainText = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);
            string Key = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);
            string Salt = KeyGenerator.GetUniqueKey(10);
            Thread.Sleep(10);

            string EncryptResult = "";
            {
                try
                {
                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = PathToConsole;
                    p.StartInfo.Arguments = $"/encrypt -k \"{Key}\" -s \"{Salt}\" -p \"{PlainText}\"";
                    p.Start();//AesEncryption
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                    EncryptResult = output.Trim();
                }
                catch (Exception err)
                {
                    throw err;
                }
            }

            string DecryptResult = "";
            {
                try
                {
                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = PathToConsole;
                    p.StartInfo.Arguments = $"/decrypt -k \"{Key}\" -s \"{Salt}\" -p \"{EncryptResult}\"";
                    p.Start();//AesEncryption
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                    DecryptResult = output.Trim();
                }
                catch (Exception err)
                {
                    throw err;
                }
            }

            Assert.AreEqual(PlainText, DecryptResult);


        }
    }
}
