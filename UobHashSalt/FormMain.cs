﻿//using EncryptionStandards;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UobHashSalt
{
    public partial class FormMain : Form
    {
        bool hourlyFactor1 = false;
        bool hourlyFactor2 = false;
        bool hourlyFactor3 = false;
        bool hourlyFactor4 = false;
        bool hourlySaltEnabled = false;

        string Salt;
        string Key;

        public void EnableHourly()
        {
            if(hourlyFactor1 && hourlyFactor2 && hourlyFactor3 && hourlyFactor4)
            {
                hourlySaltEnabled = true;
                lblHourlyMode.Visible = true;
                txtSalt.Visible = true;
            }
        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {



            var thread1 = new Thread(() =>
            {
                try
                {
                    string decidedSalt = Salt;
                    string dateTime = DateTime.UtcNow.ToString("yyyyMMddhh");
                    if (hourlySaltEnabled) decidedSalt = Salt + dateTime;

                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = "console\\EncyptionConsole.exe";
                    p.StartInfo.Arguments = $"/encrypt -k \"{Key}\" -s \"{decidedSalt}\" -p \"{txtText.Text}\"";
                    p.Start();//AesEncryption
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                    Invoke(new MethodInvoker(delegate
                    {
                        txtSalt.Text = dateTime;
                        txtResult.Text = output;
                    }));
                }
                catch (Exception err)
                {
                    txtResult.Text = "Error: " + err.Message;
                }
                //try
                //{
                //    txtResult.Text = AesEncryption.Encrypt(txtText.Text, txtKey.Text, txtSalt.Text);

                //}
                //catch (Exception err)
                //{
                //    txtResult.Text = "Error: " + err.Message;
                //}
            });
            thread1.Start();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

            Salt = ConfigurationSettings.AppSettings.Get("Salt");
            Key = ConfigurationSettings.AppSettings.Get("Key");
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {


            var thread1 = new Thread(() =>
            {

                string Salt = ConfigurationSettings.AppSettings.Get("Salt");
                string Key = ConfigurationSettings.AppSettings.Get("Key");


                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = "console\\EncyptionConsole.exe";
                p.StartInfo.Arguments = $"/decrypt -k \"{Key}\" -s \"{Salt}\" -p \"{txtText.Text}\"";
                p.Start();//AesEncryption
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                Invoke(new MethodInvoker(delegate
                {
                    txtResult.Text = output;
                }));
                //try
                //{
                //    txtResult.Text = AesEncryption.Encrypt(txtText.Text, txtKey.Text, txtSalt.Text);

                //}
                //catch (Exception err)
                //{
                //    txtResult.Text = "Error: " + err.Message;
                //}
            });
            thread1.Start();
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            hourlyFactor1 = true;
            EnableHourly();
        }

        private void pbLogo_DoubleClick(object sender, EventArgs e)
        {
            hourlyFactor2 = true;
            EnableHourly();
        }

        private void lblTitle_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            hourlyFactor3 = true;
            EnableHourly();
        }

        private void txtText_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            hourlyFactor4 = true;
            EnableHourly();
        }
    }
}
